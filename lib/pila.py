# -*- coding: utf-8 -*-

class Pila:
    """Pila tipo FIFO"""

    def __init__(self):
        self._tope = self._NodoBase()

    def apilar(self, objeto):
        self._tope = self._Nodo(objeto, self._tope)

    def desapilar(self):
        valor = self._tope.valor
        self._tope = self._tope.anterior
        return valor

    @property
    def tope(self):
        return self._tope.valor

    def esta_vacia(self):
        return self._tope.es_tope()

    class _NodoBase:
        """Nodo que reperesenta la base de una pila"""

        def es_tope(self):
            return True

        @property
        def anterior(self):
            raise ErrorDePila("Una pila vacia no tiene anterior")

        @property
        def valor(self):
            raise ErrorDePila("La pila esta vacia")


    class _Nodo:
        """Nodo de una pila"""

        def __init__(self, objeto, anterior):
            self.valor = objeto
            self.anterior = anterior

        def es_tope(self):
            return False

class ErrorDePila(StandardError):
    u"""Clase que significa que ocurrió un error en el manejo de una pila"""
