require 'erubis'


class Ruta

  def initialize(ruta)
    @ruta = ruta
  end

  def ruta
    @ruta
  end

  def segura
    @ruta.gsub('_', '\_')
  end
end

rutas = Dir['../**/*.py'].reject{ |ruta| ruta.include? 'informe' }.map { |ruta| Ruta.new(ruta) }


File.open 'codigo.tex.erb' do |fuente|
  render = Erubis::Eruby.new(fuente.read).result({rutas: rutas})
  File.open 'codigo.tex', 'w' do |destino|
    destino.write render
  end
end
