#!/usr/bin/env python
# -*- coding: utf-8 -*-

from app.maquina_virtual import MaquinaVirtual
from app.maquina_virtual_debug import MaquinaVirtualDebug
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Correr un programa')
    parser.add_argument('programa', type=str, help='Ruta hacia el programa.')
    parser.add_argument('--debug', '-d', action='store_true',
                        help='run program in debug mode')
    args = parser.parse_args()

    if args.debug:
        vm = MaquinaVirtualDebug()
    else:
        vm = MaquinaVirtual()

    vm.cargar(args.programa)
    vm.correr()
