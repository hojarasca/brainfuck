#!/usr/bin/env python
# -*- coding: utf-8 -*-

from app.programa import Programa
from app.error.error_de_sintaxis import ErrorDeSintaxis
import pytest
from StringIO import StringIO

class TestProgramaTerminado:

    def test_cuando_el_codigo_fuente_es_un_string_vacio_el_programa_termino(self):
        programa = Programa("")
        assert programa.esta_terminado() == True


    def test_cuando_el_codigo_fuente_no_es_vacio_el_programa_no_esta_terminado(self):
        programa = Programa("<")
        assert programa.esta_terminado() == False


    def test_cuando_el_cf_tiene_solo_comentarios_el_programa_ya_termino(self):
        programa = Programa("lala")
        assert programa.esta_terminado() == True

    def test_cuando_el_cf_contiene_codigo_y_comentarios_el_programa_no_termino(self):
        programa = Programa("Esto es < que esto otro")
        assert programa.esta_terminado() == False

    def test_cuado_el_programa_tiene_instrucciones_y_ya_se_ejecutaron_el_programa_esta_terminado(self):
        programa = Programa("<<")
        programa.ejecutar_instruccion_actual()
        programa.ejecutar_instruccion_actual()
        assert programa.esta_terminado() == True


class TestInstrucciones:

    def test_un_programa_que_solo_suma_1_afecta_la_memoria_sumando_uno_en_la_primera_posicion(self):
        programa = Programa("+")
        programa.ejecutar_instruccion_actual()
        assert programa.memoria()[0] == 1


    def test_mover_derecha_y_sumar_uno_deja_la_memoria_en_0_en_prim_pos_y_2_en_seg_pos(self):
        programa = Programa(">+")
        programa.ejecutar_instruccion_actual()
        programa.ejecutar_instruccion_actual()
        assert programa.memoria()[0] == 0
        assert programa.memoria()[1] == 1


    def test_sumar_avanzar_sumar_retroceder_sumar(self):
        """ +>+<+ deja la memoria con 2 en al pos 0 y 1 en la pos 1"""

        programa = Programa("+>+<+")
        while not programa.esta_terminado():
            programa.ejecutar_instruccion_actual()

        assert programa.memoria()[0] == 2
        assert programa.memoria()[1] == 1

    def test_cuando_se_resta_en_una_casilla_en_cero_esta_queda_en_255(self):
        programa = Programa("-")
        programa.ejecutar_instruccion_actual()

        assert programa.memoria()[0] == 255


    def test_cuando_se_suma_y_resta_1_a_la_misma_celda_el_contenido_final_es_cero(self):
        programa = Programa("+-")
        programa.ejecutar_instruccion_actual()
        programa.ejecutar_instruccion_actual()

        assert programa.memoria()[0] == 0

    def test_cuando_se_resta_y_suma_1_a_la_misma_celda_el_resultado_final_es_cero(self):
        programa = Programa("-+")
        programa.ejecutar_instruccion_actual()
        programa.ejecutar_instruccion_actual()

        assert programa.memoria()[0] == 0

    #Instrucciones [ y ]

    def test_un_programa_con_corchetes_abiertos_pero_no_cerrados_no_compila(self):
        with pytest.raises(ErrorDeSintaxis) as e:
            Programa("[")
        assert "Todos los corchetes deben tener un par" in e.value.message


    def test_un_programa_que_cierra_un_bucle_no_abierto_no_compila(self):
        with pytest.raises(ErrorDeSintaxis) as e:
            Programa("]")
        assert "Todos los corchetes deben tener un par" in e.value.message


    def test_los_corchetes_tienen_que_tener_pares_con_sentido(self):
        """El programa '][' tien un corchete de cada tipo, sin embargo no
        es un programa correcto porque no se puede establecer un punto
        de returno para el corchete cerrado ni un punto de salida para el
        corchete abierto"""

        with pytest.raises(ErrorDeSintaxis) as e:
            Programa("][")
        assert "Todos los corchetes deben tener un par" in e.value.message


    def test_bucle_sencillo(self):
        """El progama '+++[>+<-]' debe dejar la posicion 0 de la memoria
        en 0 en la posicion 1 de la memoria en 3"""
        programa = Programa("+++[>+<-]")

        while not programa.esta_terminado():
            programa.ejecutar_instruccion_actual()

        assert programa.memoria()[0] == 0
        assert programa.memoria()[1] == 3


    def test_bucles_anidados(self):
        """El programa '+++[>+++[>+<-]<-]' deberia dejar la posicion 0 en 0,
        la 1 en 0 y la 2 en 9"""
        programa = Programa("+++[>+++[>+<-]<-]")

        while not programa.esta_terminado():
            programa.ejecutar_instruccion_actual()

        assert programa.memoria()[0] == 0
        assert programa.memoria()[1] == 0
        assert programa.memoria()[2] == 9


    #Instruccion de salida por pantalla

    def test_la_instruccion_punto_imprime_el_ascii_de_la_celda_actual(self):
        """Para este test se va a usar el programa
        '++++++++[>++++++++<-]>.' que suma 64 en la posicion 1 de la memoria
        y a continuación imprime el contenido de la posición 1, que interpretado
        como ascii es una '@'."""
        buf = StringIO()
        programa = Programa("++++++++[>++++++++<-]>.", buf)

        while not programa.esta_terminado():
            programa.ejecutar_instruccion_actual()
        assert buf.getvalue() == '@'

