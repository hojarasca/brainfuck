# -*- coding: utf-8 -*-

from .programa import Programa

class MaquinaVirtual:

    def __init__(self):
        self._programa = None


    def cargar(self, ruta):
        with open(ruta, 'r') as archivo:
            self._programa = Programa(archivo.read())


    def correr(self):
        while not self._programa.esta_terminado():
            self._programa.ejecutar_instruccion_actual()
