# -*- coding: utf-8 -*-

from .instrucciones.avanzar import Avanzar
from .instrucciones.retroceder import Retroceder
from .instrucciones.incrementar import Incrementar
from .instrucciones.disminuir import Disminuir
from .instrucciones.mientras import Mientras
from .instrucciones.retornar import Retornar
from .instrucciones.imprimir import Imprimir
from .instrucciones.recibir import Recibir
from .instrucciones.instruccion_final import InstruccionFinal
from .error.error_de_sintaxis import ErrorDeSintaxis
from .memoria import Memoria
from lib.pila import Pila, ErrorDePila

import sys

class Programa:

    INSTRUCCIONES_CONOCIDAS = {instruccion.simbolo():instruccion for instruccion in [
            Retroceder,
            Avanzar,
            Incrementar,
            Disminuir,
            Mientras,
            Retornar,
            Imprimir,
            Recibir
        ]
    }


    def __init__(self, codigo_fuente, salida=sys.stdout, entrada=sys.stdin,
                 set_de_instrucciones=INSTRUCCIONES_CONOCIDAS):
        self.set_de_instrucciones = set_de_instrucciones
        self._primera_instruccion = self._precompilar_programa(codigo_fuente)
        self._instruccion_actual = self._primera_instruccion
        self._memoria = Memoria()
        self._puntero_memoria = 0
        self.salida = salida
        self.entrada = entrada

    def ejecutar_instruccion_actual(self):
        self._instruccion_actual.ejecutar(self)
        self._instruccion_actual = self._instruccion_actual.proxima_instruccion


    def esta_terminado(self):
        return self._instruccion_actual.esta_terminado()

    def memoria(self):
        return self._memoria

    def _precompilar_programa(self, codigo_fuente):
        codigo_fuente = codigo_fuente[::-1]
        pila = Pila()
        siguiente = InstruccionFinal()
        for caracter in codigo_fuente:
            if caracter in self.set_de_instrucciones:
                nueva_instruccion =  self.set_de_instrucciones[caracter](siguiente)
                if caracter == Retornar.simbolo():
                    pila.apilar(nueva_instruccion)
                if caracter == Mientras.simbolo():
                    try:
                        final_ciclo = pila.desapilar()
                    except(ErrorDePila):
                        raise ErrorDeSintaxis("Todos los corchetes deben tener un par")

                    final_ciclo.punto_de_retorno = nueva_instruccion
                    nueva_instruccion.punto_de_salida = final_ciclo
                siguiente = nueva_instruccion

        if not pila.esta_vacia():
            raise ErrorDeSintaxis("Todos los corchetes deben tener un par")
        return siguiente


    def goto(self, instruccion):
        self._instruccion_actual = instruccion


    def bytecode(self):
        bytecode = "".join([instruccion.simbolo() for instruccion in \
                                                    self._instrucciones()])

        return bytecode

    def _instrucciones(self):
        lista = []
        actual = self._primera_instruccion
        while not actual.esta_terminado():
            lista.append(actual)
            actual = actual.proxima_instruccion

        lista.append(actual)
        return lista

    def instruccion_actual(self):
        return self._instruccion_actual

    def indice_instruccion_actual(self):
        return self._instrucciones().index(self._instruccion_actual)
