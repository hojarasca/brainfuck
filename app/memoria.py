# -*- coding: utf-8 -*-


class Memoria:
    def __init__(self):
        self._contenido = {}
        self._puntero = 0


    def leer(self):
        return self._contenido.get(self._puntero, 0)

    def escribir(self, char):
        self._contenido[self._puntero] = ord(char)


    def avanzar(self):
        self._puntero +=1


    def retroceder(self):
        self._puntero -=1


    def incrementar(self):
        self._contenido[self._puntero] = \
            (self._contenido.get(self._puntero, 0) + 1) % 256


    def disminuir(self):
        self._contenido[self._puntero] = \
            (self._contenido.get(self._puntero, 0) - 1) % 256


    def __getitem__(self, indice):
        return self._contenido.get(indice, 0)

    def posicion_actual(self):
        return self._puntero
