# -*- coding: utf-8 -*-

import curses

class VentanaPrograma:

    def __init__(self, ventana, codigo):
        self.borde = ventana
        self.borde.border()
        self.borde.addstr(0, 2, 'Codigo en ejecucion')
        medidas = ventana.getmaxyx()
        self.ventana = ventana.derwin(medidas[0] - 2, medidas[1] - 2, 1, 1)
        self.codigo = " "*56 + codigo + " "*56
        self.ventana.border()

    def actualizar(self, pos_actual):
        self.ventana.clear()
        pos_actual += 56
        inicio = pos_actual - 56
        fin = pos_actual + 56
        self.ventana.addstr(0, 0, self.codigo[inicio:fin])
        self.ventana.chgat(0, 56, 1, curses.color_pair(1))
        self.ventana.refresh()
