# -*- coding: utf-8 -*-

import curses

class VentanaMemoria:

    def __init__(self, ventana, memoria):
        self.borde = ventana
        self.borde.border()
        self.borde.addstr(0, 2, 'Ventana Memoria 0xx')
        medidas = ventana.getmaxyx()
        self.ventana = ventana.derwin(medidas[0] - 2, medidas[1] - 2, 1, 1)
        self.memoria = memoria

    def actualizar(self):
        memoria_como_string = self.renderizar_memoria()
        self.ventana.addstr(0, 0, memoria_como_string)
        self.ventana.chgat(0, 56, 1, curses.color_pair(1))
        self.ventana.refresh()


    def renderizar_memoria(self):
        pos_actual = self.memoria.posicion_actual()
        rango = range( pos_actual - 14, pos_actual + 15)
        self.borde.addstr(0, 2, 'Ventana Memoria {}xx'.format(pos_actual / 100))
        fila_posiciones = self.renderizar_posiciones(rango)
        fila_contenido = "|".join(["{0:<3d}".format(self.memoria[pos]) for pos in rango])
        return "\n".join([fila_posiciones, fila_contenido])


    def renderizar_posiciones(self, rango):
        posiciones = []
        for pos in rango:
            if pos != 0:
                signo = pos/abs(pos)
                posiciones.append("{0:<3d}".format(pos % (100 * signo)))
            else:
                posiciones.append("0  ")

        return "|".join(posiciones)

