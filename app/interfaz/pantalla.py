# -*- coding: utf-8 -*-

import curses
import sys
from .ventana_programa import VentanaPrograma
from .ventana_salida import VentanaSalida
from .ventana_memoria import VentanaMemoria
from .ventana_respuesta import VentanaRespuesta

class Pantalla:

    COMANDOS = {
        '': lambda i, *args: i.ejecutar_una_instruccion(),
        's': lambda i, *args: i.ejecutar_una_instruccion(),
        'h': lambda i, *args: i.mostrar_ayuda(),
        'm': lambda i, *args: i.mostrar_posicion_memoria(*args),
        'c': lambda i, *args: i.continuar(),
        'q': lambda i, *args: i.terminar()
    }

    def __init__(self, vm, salida):
        self.vm = vm
        self.salida = salida

    def inicializar_pantalla(self):
        curses.initscr()
        self.pantalla_principal = curses.newwin(25, 119)
        self.ventana_programa = VentanaPrograma(
                                    self.pantalla_principal.subwin(3, 119, 0, 0),
                                    self.vm.codigo_fuente_actual()
        )

        self.ventana_salida = VentanaSalida(
                                self.pantalla_principal.subwin(10, 119, 3, 0),
                                self.salida
        )

        self.ventana_memoria = VentanaMemoria(
                                self.pantalla_principal.subwin(4, 119, 13, 0),
                                self.vm.memoria_actual()
        )

        self.ventana_respuesta = VentanaRespuesta(
                                    self.pantalla_principal.subwin(6, 119, 18, 0)
        )

        curses.noecho()
        self.pantalla_principal.addstr(24, 0, self.PROMPT)
        curses.start_color()
        curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
        self.pantalla_principal.keypad(1)


    def main(self):
        self.inicializar_pantalla()
        try:
            while not self.vm.programa_terminado():
                self.mostrar_estado()
                self.recibir_comando()
            self.ventana_respuesta.respuesta = "El programa ha terminado con \
exito. Presione cualquier tecla para salir"
            self.mostrar_estado()
            self.pantalla_principal.getch()
        finally:
            self.finalizar()


    PROMPT = "ncdb>"

    def recibir_comando(self):
        curses.echo()
        entrada = self.pantalla_principal.getstr(24, len(self.PROMPT))
        curses.noecho()
        self.pantalla_principal.hline(24,0, ' ', 119)
        self.pantalla_principal.addstr(24, 0, self.PROMPT)
        entrada = entrada.split(' ')
        accion = self.COMANDOS.get(entrada[0], lambda param, *args: None)
        accion(self, *entrada)



    def terminar(self):
        sys.exit()


    def mostrar_ayuda(self):
        ayuda = """Comandos:
s o '': ejecutar una instrucion                                       c: Continua hasta el proximo breakpoint.
m POS: muestra el contenido de la memoria en la posicion POS     q: Termina el programa y sale del debugger.
h: muestra esta ayuda"""
        self.ventana_respuesta.respuesta = ayuda

    def ejecutar_una_instruccion(self):
        self.vm.ejecutar_una_instruccion()

    def continuar(self):
        self.vm.continuar()

    def mostrar_posicion_memoria(self, *args):
        if len(args) != 2:
            self.ventana_respuesta.respuesta = "Se debe especificar una \
unica posicion de memoria"
            return
        try:
            pos = int(args[1])
        except:
            self.ventana_respuesta.respuesta = "La posicion de memoria debe ser \
un entero"
            return
        contenido = self.vm.memoria_actual()[pos]
        self.ventana_respuesta.respuesta = "Contenido posicion {}: {}"\
                                                    .format(pos, contenido)


    def finalizar(self):
        curses.endwin()


    def mostrar_estado(self):
        self.ventana_programa.actualizar(self.vm.puntero_insrucciones())
        self.ventana_salida.actualizar()
        self.ventana_memoria.actualizar()
        self.ventana_respuesta.actualizar()

