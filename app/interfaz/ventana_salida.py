# -*- coding: utf-8 -*-

class VentanaSalida:

    def __init__(self, ventana, buf):
        self.borde = ventana
        self.borde.border()
        self.borde.addstr(0, 2, 'Salida standard')
        medidas = ventana.getmaxyx()
        self.ventana = ventana.derwin(medidas[0] - 1, medidas[1] - 2, 1, 1)
        self.buf = buf


    def actualizar(self):
        texto = "\n".join(self.buf.getvalue().split("\n")[-9:])
        self.ventana.addstr(0, 0, texto)
        self.ventana.refresh()

