# -*- coding: utf-8 -*-

class VentanaRespuesta:

    def __init__(self, ventana):
        self.ventana = ventana
        self.respuesta = ""


    def actualizar(self):
        self.ventana.clear()
        filas = self.respuesta.split("\n")
        cant_filas = len(filas)
        filas = [""]*(max(0, 6 - cant_filas)) + filas
        texto = "\n".join(filas)
        self.ventana.addstr(0, 0, texto)
        self.ventana.refresh()


