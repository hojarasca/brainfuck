# -*- coding: utf-8 -*-

from .programa import Programa
from .instrucciones.breakpoint import Breakpoint
from StringIO import StringIO
from .interfaz.pantalla import Pantalla


class MaquinaVirtualDebug:

    def __init__(self):
        self._programa = None
        self._salida = StringIO()


    def cargar(self, ruta):
        instrucciones = dict(Programa.INSTRUCCIONES_CONOCIDAS)
        instrucciones[Breakpoint.simbolo()] = Breakpoint
        with open(ruta, 'r') as archivo:
            self._programa = Programa(archivo.read(), salida=self._salida,
                                      set_de_instrucciones=instrucciones)


    def correr(self):
        interfaz = Pantalla(self, self._salida)
        interfaz.main()


    def codigo_fuente_actual(self):
        return self._programa.bytecode()


    def puntero_insrucciones(self):
        return self._programa.indice_instruccion_actual()


    def memoria_actual(self):
        return self._programa.memoria()


    def programa_terminado(self):
        return self._programa.esta_terminado()


    def ejecutar_una_instruccion(self):
        self._programa.ejecutar_instruccion_actual()


    def continuar(self):
        self._programa.ejecutar_instruccion_actual()
        while self._programa.instruccion_actual().simbolo() != Breakpoint.simbolo() and\
                                                 not self._programa.esta_terminado():
            self._programa.ejecutar_instruccion_actual()


