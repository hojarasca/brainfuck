# -*- coding: utf-8 -*-

from .instruccion import Instruccion

class Avanzar(Instruccion):

    @classmethod
    def simbolo(cls):
        return ">"


    def ejecutar(self, programa):
        """Incrementa en uno la posicion actual de memoria"""
        programa.memoria().avanzar()


