# -*- coding: utf-8 -*-

from .instruccion import Instruccion

class Breakpoint(Instruccion):
    u"""Instrucción vacía que solo sirve para indicarle al debugger
    que debe parar la ejecución del programa"""

    @classmethod
    def simbolo(cls):
        return "~"


    def ejecutar(self, programa):
        """No hace nada"""
        pass


