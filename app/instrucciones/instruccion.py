# -*- coding: utf-8 -*-

from abc import ABCMeta, abstractmethod

class Instruccion:
    __metaclass__ = ABCMeta

    def __init__(self, proxima_instruccion):
        self.proxima_instruccion = proxima_instruccion


    @abstractmethod
    def ejecutar(self, programa):
        """Método abstracto que ejecuta la instruccion."""

    @abstractmethod
    def simbolo(self):
        """Simbolo que representa a la instruccion en el código fuente
        IMPORTANTE: Este método se DEBE definir como método de CLASE"""


    def esta_terminado(self):
        return False

    def __str__(self):
        return "<Instruccion: {} >".format(self.simbolo())
