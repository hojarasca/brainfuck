# -*- coding: utf-8 -*-

from .instruccion import Instruccion


class InstruccionFinal(Instruccion):

    @classmethod
    def simbolo(cls):
        return ''

    def __init__(self):
        """Sobre escribo el inicializador normal de instruccion"""

    def ejecutar(self, programa):
        raise RuntimeError("No se puede seguir ejecutando un programa terminado")


    @property
    def proxima_instruccion(self):
        raise RuntimeError("El programa ya esta terminado. No hay proxima instruccion")

    def esta_terminado(self):
        return True



