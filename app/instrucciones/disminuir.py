# -*- coding: utf-8 -*-

from .instruccion import Instruccion

class Disminuir(Instruccion):

    @classmethod
    def simbolo(cls):
        return "-"


    def ejecutar(self, programa):
        programa.memoria().disminuir()


