# -*- coding: utf-8 -*-

from .instruccion import Instruccion

class Mientras(Instruccion):

    @classmethod
    def simbolo(cls):
        return "["

    def __init__(self, proxima_instruccion):
        self.proxima_instruccion = proxima_instruccion
        self.punto_de_salida = None

    def ejecutar(self, programa):
        if programa.memoria().leer() == 0:
            programa.goto(self.punto_de_salida)
