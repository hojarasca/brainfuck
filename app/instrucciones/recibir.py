# -*- coding: utf-8 -*-

from .instruccion import Instruccion

class Recibir(Instruccion):

    @classmethod
    def simbolo(cls):
        return ","


    def ejecutar(self, programa):
        """Imprime el contenido de la celda actual de memoria
        por la salida standard"""
        programa.memoria().escribir(programa.entrada.read(1))

